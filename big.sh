#!/bin/bash

#script care returneaza cele mai mari 5 fisiere

#preiau locatia unde trebuie verificat
file=$1

if [[ ! -d $1 ]]; then
    echo "Argumentul trebuie sa fie director"
    exit
fi

#printez fisierele in ordine
du -a /$1 | sort -n -r | head -n 6 | awk '{ print $2 }'
