1. Pentru scriptul directory.sh m-am gandit sa cer user input pentru a face numarul de directoare dorit. Am facut inclusiv verificarea daca ce e introdus este integer.
2. Pentru scriptul empty_files.sh am folosit comanda find si parametrii -empty -type si -delete pentru a sterge automat fisierele goale.
3. Pentru scriptul info.sh am printat hostname-ul direct, la IP am folosit ip -br a si am extras cu awk de la linia 2 in jos (ca sa scap de localhost) si coloana 3, unde se afla IP-ul efectiv. La Uptime am mers pe aceeasi idee si am printat coloana 2 si cu sed am eliminat virgula. Am folosit grep -ohe pentru a gasi unde scrie up. Pentru Current User am folosit who si am printat prima coloana cu awk., 
4. Pentru scriptul upgrade.sh am folosit sudo apt-get update pentru update-ul efectiv si sudo apt-get -y autoremove, sudo apt-get autoclean pentru a sterge pachetele inlocuite.
5. Pentru automate.sh am folosit touch pentru a face fisierul test.sh. Apoi am folosit echo pentru shebang, chmod cu drepturi full, am instalat vim cu sudo apt-get install vim si apoi vim test.sh pentru editare.

6. Pentru change_ext.sh am facut un if care sa verifice ca numarul de argumente este cel necesat si un if care sa verifice ca primul argument este un director. Apoi urmeaza redenumirea efectiva, iar daca nu se redenumeste niciun fisier userul este anuntat.
7. Pentru find_os.sh am pfolosit grep pe /etc/os-release sa caut dupa ID_LIKE si apoi cu cut am pastrat doar partea cu ditro-ul. Apoi cu un if am verificat daca este Debian sau CentOS si apoi am instalat git si vim in functie de package manager.
8. Pentru group.sh am mers pe ideea sa verific daca exista grupul/userul si daca nu exista sa il creeze.
9. Pentru type.sh am preluat fisierul ca argument, am printat informatii despre el cu file si apoi am verificat daca este executabil.
10. Pentru big.sh am preluat folderul ca argument (l-am verificat daca este director) si apoi am printat fisierele in ordinea dimensiunilor.
11. Pentru backtrack.sh preiau numarul N ca argument (il verifica daca este un numar) si apoi se duce cu N foldere in urma. Ca sa mearga trebuie apelat cu source numele_scriptului.sh .
