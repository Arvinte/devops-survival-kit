#!/bin/bash

read -p "Numarul de directoare: " nr

if [[ $nr ]] && [ $nr -eq $nr 2>/dev/null ]
then
	while [ $nr != 0 ]
	do
		mkdir "dir"$nr
		nr=$(( nr-1 ))
	done
else
	echo "Pune un numar, te rog :D"
fi
