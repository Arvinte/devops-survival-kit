#!/bin/bash

#script care returneaza tipul de fisier si daca e executabil

#preiau fisierul ca argument
file=$1

#printez tipul fisierului
echo $(file $file)

#verific daca fisierul e executabil
if [[ -x "$file" ]]
then
	echo "File '$file' is executable" 
fi
