#!/bin/bash

#script care verifica daca exista grupul devops sau userul engineer si daca nu exista le creeaza


#verific daca exista si daca exista preiau devops
group=$(less /etc/group | grep "devops"| cut -c 1-6)

#verific daca exista si daca exista preiau engineer
user=$(awk -F: '{ print $1 }' /etc/passwd | grep "engineer")

#verific daca exista devops

if [ "$group" == "devops" ]; then
	echo "DevOps group exists!"
else
	sudo groupadd devops
fi

#verific daca exista engineer

if [ "$user" == "engineer" ]; then
	echo "User engineer already exists!"
else
	sudo useradd engineer
fi
