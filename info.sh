#!/bin/bash

echo "Hostname: "
hostname
echo " "

echo "IP: "
ip -br a | awk 'NR>1 { print $3 }'
echo " "

echo "Uptime: "
uptime | grep -ohe 'up .*' | sed 's/,//g' | awk '{ print $2 }'
echo " "

echo "Current user: "
who | awk '{ print $1 }'
echo " "
